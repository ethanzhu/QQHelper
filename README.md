#QQHelper QQ小助手  by 三少要怒沥出惩淉（764724624@qq.com）
##感谢以下参与项目成员：##
**chenxehua (763159993@qq.com)**<br>
**月魄霜 (1140625494@qq.com)**<br>

## 功能 ##
  * 消息助手（给群和好友群发消息）<br> 
  * 通讯录助手（批量导出好友联系方式）<br> 

## 运行环境 ##
*注：本文假设你已经有vs2010的开发环境*<br>
启动vs2010，导入项目，本项目是一个WebApp<br>

下面将简单的解析下项目：

## **一、项目的目录结构** ##
> 根目录<br>
> ├ App_Code---c#源码<br>
> ├ Bin---包含json、yiwoSDK .net库<br>
> ├ js---包含jquery库<br>
> ├ v2---基于dojo mobile库的v2版本<br>
> ├ Services.asmx---web服务<br>
> ├ index.html---默认应用页<br>

## **二、提示** ##
开发更多功能请详细查看Bin\yiwoSDK.chm接口文档，在App_Code\Service.cs中加入自己的服务。